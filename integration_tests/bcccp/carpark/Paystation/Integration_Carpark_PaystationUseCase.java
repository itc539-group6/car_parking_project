package bcccp.carpark.Paystation;

import bcccp.carpark.Carpark;
import bcccp.carpark.ICarpark;
import bcccp.carpark.paystation.IPaystationController;
import bcccp.carpark.paystation.IPaystationUI;
import bcccp.carpark.paystation.PaystationController;
import bcccp.carpark.paystation.PaystationUI;
import bcccp.tickets.adhoc.AdhocTicketDAO;
import bcccp.tickets.adhoc.AdhocTicketFactory;
import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicketDAO;
import bcccp.tickets.season.SeasonTicketDAO;
import bcccp.tickets.season.UsageRecordFactory;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class Integration_Carpark_PaystationUseCase {
    IAdhocTicketDAO adhocdao;
    ISeasonTicketDAO seasondao;
    ICarpark carpark;
    IPaystationUI mockUI;
    IPaystationController payController;
    IAdhocTicket ticket;

    @Test
    public void testTicketInserted() {
        //set up DAO's and Carpark
        adhocdao = new AdhocTicketDAO(new AdhocTicketFactory());
        seasondao = new SeasonTicketDAO(new UsageRecordFactory());
        carpark = new Carpark("test carpark", 10, 1, adhocdao, seasondao);

        //set up mock Paystation UI,
        mockUI = mock(PaystationUI.class);

        //paystation controller
        payController = new PaystationController(carpark, mockUI);

        ticket = carpark.issueAdhocTicket();
        ticket.enter(System.currentTimeMillis());

        payController.ticketInserted(ticket.getBarcode());

        verify(mockUI).display(Mockito.contains("Pay "));
        verify(mockUI, never()).beep();

    }

    @Test
    public void testTicketPaid() throws InterruptedException {
        //set up DAO's and Carpark
        adhocdao = new AdhocTicketDAO(new AdhocTicketFactory());
        seasondao = new SeasonTicketDAO(new UsageRecordFactory());
        carpark = new Carpark("test carpark", 10, 1, adhocdao, seasondao);

        //set up mock Paystation UI,
        mockUI = mock(PaystationUI.class);

        //paystation controller
        payController = new PaystationController(carpark, mockUI);

        ticket = carpark.issueAdhocTicket();
        ticket.enter(System.currentTimeMillis());

        payController.ticketInserted(ticket.getBarcode());

        //Implemented second delay to avoid paidTime equalling entryTime
        TimeUnit.SECONDS.sleep(1);

        payController.ticketPaid();

        assertTrue(ticket.getPaidDateTime() > 0);
        verify(mockUI, never()).beep();
    }
}
