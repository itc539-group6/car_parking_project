package bcccp.carpark.entry;

import bcccp.carpark.*;
import bcccp.carpark.entry.EntryController;
import bcccp.carpark.entry.IEntryUI;
import bcccp.tickets.adhoc.AdhocTicket;
import org.junit.*;
import org.junit.runners.MethodSorters;

import static org.mockito.Mockito.*;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EntryControllerTest {

    static Carpark mockCarpark;
    static IGate dummyGate;
    static ICarSensor mockOS;
    static ICarSensor mockIS;
    static IEntryUI mockUI;

    static EntryController seasonInstance;
    static EntryController adhocInstance;
    static EntryController isFullInstance;


    public EntryControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        mockCarpark = mock(Carpark.class);
        dummyGate = mock(IGate.class);
        mockOS = mock(ICarSensor.class);
        mockIS = mock(ICarSensor.class);
        mockUI = mock(IEntryUI.class);


        when(mockOS.getId()).thenReturn("OutsideSensor");
        when(mockOS.carIsDetected()).thenReturn(true);


        seasonInstance = new EntryController(mockCarpark, dummyGate, mockOS, mockIS, mockUI);
        adhocInstance = new EntryController(mockCarpark, dummyGate, mockOS, mockIS, mockUI);
        isFullInstance = new EntryController(mockCarpark, dummyGate, mockOS, mockIS, mockUI);

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    @Test
    public void test1ButtonPushed() {
        System.out.println("buttonPushed");


        when(mockCarpark.isFull()).thenReturn(false);


        AdhocTicket ticket = mock(AdhocTicket.class);
        when(mockCarpark.issueAdhocTicket()).thenReturn(ticket);


        adhocInstance.carEventDetected("OutsideSensor", true);


        adhocInstance.buttonPushed();


        when(mockCarpark.isFull()).thenReturn(true);
        isFullInstance.carEventDetected("OutsideSensor", true);


        isFullInstance.buttonPushed();


        verify(mockUI, times(1)).display("Take Ticket");
        verify(mockUI, times(1)).display("Carpark Full");

        verify(mockUI, never()).beep();

    }


    @Test
    public void test2TicketInserted() {
        System.out.println("ticketInserted");


        String barcode = "S1111";

        when(mockCarpark.isSeasonTicketValid(barcode)).thenReturn(true);
        when(mockCarpark.isSeasonTicketInUse(barcode)).thenReturn(false);


        seasonInstance.carEventDetected("OutsideSensor", true);
        seasonInstance.ticketInserted(barcode);


        verify(mockUI, times(1)).display("Ticket Validated");

        verify(mockUI, never()).beep();
    }


    @Test
    public void test3TicketTaken() {
        System.out.println("ticketTaken");


        seasonInstance.ticketTaken();
        adhocInstance.ticketTaken();


        verify(mockUI, times(2)).display("Ticket Taken");

        verify(mockUI, never()).beep();
    }


    @Test
    public void test4NotifyCarparkEvent() {
        System.out.println("notifyCarparkEvent");


        when(mockCarpark.isFull()).thenReturn(false);


        isFullInstance.notifyCarparkEvent();


        verify(mockUI, times(4)).display("Push Button");

        verify(mockUI, never()).beep();
    }


    @Test
    public void test5CarEventDetected() {
        System.out.println("carEventDetected");


        when(mockOS.carIsDetected()).thenReturn(false);
        EntryController carEventInstance = new EntryController(mockCarpark, dummyGate, mockOS, mockIS, mockUI);

        when(mockOS.getId()).thenReturn("OutsideSensor");
        when(mockIS.getId()).thenReturn("InsideSensor");


        carEventInstance.carEventDetected("InsideSensor", true);
        carEventInstance.carEventDetected("InsideSensor", false);


        when(mockOS.carIsDetected()).thenReturn(true);
        carEventInstance.carEventDetected("OutsideSensor", true);


        carEventInstance.carEventDetected("InsideSensor", true);
        carEventInstance.carEventDetected("InsideSensor", false);


        when(mockOS.carIsDetected()).thenReturn(false);
        carEventInstance.carEventDetected("OutsideSensor", false);


        when(mockOS.carIsDetected()).thenReturn(true);
        carEventInstance.carEventDetected("OutsideSensor", true);
        carEventInstance.buttonPushed();
        carEventInstance.ticketTaken();


        when(mockOS.carIsDetected()).thenReturn(false);
        carEventInstance.carEventDetected("OutsideSensor", false);


        when(mockOS.carIsDetected()).thenReturn(true);
        carEventInstance.carEventDetected("OutsideSensor", true);
        carEventInstance.buttonPushed();
        carEventInstance.ticketTaken();


        carEventInstance.carEventDetected("InsideSensor", true);


        carEventInstance.carEventDetected("InsideSensor", false);
        carEventInstance.carEventDetected("InsideSensor", true);


        when(mockOS.carIsDetected()).thenReturn(false);
        carEventInstance.carEventDetected("OutsideSensor", false);


        carEventInstance.carEventDetected("OutsideSensor", true);
        carEventInstance.carEventDetected("OutsideSensor", false);


        carEventInstance.carEventDetected("InsideSensor", false);


        verify(mockUI, times(8)).display("Push Button");

        verify(mockUI, times(2)).display("Blocked");

        verify(mockUI, times(5)).display("Ticket Taken");

        verify(mockUI, times(3)).display("Entering");

        verify(mockUI, times(2)).display("Entered");

        verify(mockUI, never()).beep();
    }

}