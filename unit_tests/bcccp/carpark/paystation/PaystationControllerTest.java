package bcccp.carpark.paystation;

import bcccp.carpark.Carpark;
import bcccp.tickets.adhoc.AdhocTicket;
import org.junit.*;
import org.junit.runners.MethodSorters;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PaystationControllerTest {

    static Carpark mockCarpark;
    static IPaystationUI mockUI;
    static PaystationController instance;

    public PaystationControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        mockCarpark = mock(Carpark.class);
        mockUI = mock(IPaystationUI.class);

        //create a static PaystationController instance for testing multiple methods in sequence
        instance = new PaystationController(mockCarpark, mockUI);
    }


    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1TicketInserted() {
        System.out.println("ticketInserted");

        //Initialise behaviour of AdhocTicket mocks
        String barcode = "A1111";
        AdhocTicket ticket = mock(AdhocTicket.class);
        when(mockCarpark.getAdhocTicket(barcode)).thenReturn(ticket);
        when(mockCarpark.calculateAdhocTicketCharge(any(long.class))).thenReturn(10.00f);

        //Begin ticketInserted test
        instance.ticketInserted(barcode);

        //Test that the mockUI system entered the correct states as a result of this method.
        verify(mockUI, times(1)).display("Pay 10.00");
        //a beep indicates an error that may not have been caught otherwise.
        verify(mockUI, never()).beep();
    }

    @Test
    public void test2TicketPaid() {
        System.out.println("ticketPaid");

        //Begin ticketPaid test
        instance.ticketPaid();

        //Test that the mockUI system entered the correct states as a result of this method.
        verify(mockUI, times(1)).display("Paid");
        //a beep indicates an error that may not have been caught otherwise.
        verify(mockUI, never()).beep();
    }


    @Test
    public void test3TicketTaken() {
        System.out.println("ticketTaken");

        instance.ticketTaken();

        //mockUI controller should have become idle when initialised, and again now (total 2 times).
        verify(mockUI, times(1)).display("Paid");
        //a beep indicates an error that may not have been caught otherwise.
        verify(mockUI, never()).beep();
    }

}